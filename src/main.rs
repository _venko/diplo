use std::{fmt::Display, str::FromStr};
use rustyline::{sqlite_history::SQLiteHistory, Config, Editor};
use forth_parser::Parser;

mod dictionary;
mod execute;
// mod parser;

use crate::{
    dictionary::{Dict, Instr, Word},
    execute::execute_word,
};

#[derive(Default, Debug, Clone, Copy, PartialEq, Eq)]
enum StackMode {
    #[default]
    Param,
    Word,
}

impl StackMode {
    pub fn swap(&self) -> StackMode {
        match self {
            StackMode::Param => StackMode::Word,
            StackMode::Word => StackMode::Param,
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
enum Param {
    Bool(bool),
    Num(f32),
    String(String),
    Word(Word),
}

impl Display for Param {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Param::Bool(b) => write!(f, "{}", b),
            Param::Num(n) => write!(f, "{}", n),
            Param::String(s) => write!(f, "{}", &s),
            Param::Word(w) => write!(f, "<word {}>", w.name),
        }
    }
}

struct ParamFromStrError;

impl FromStr for Param {
    type Err = ParamFromStrError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Ok(b) = s.parse::<bool>() {
            Ok(Param::Bool(b))
        } else if let Ok(n) = s.parse::<f32>() {
            Ok(Param::Num(n))
        } else if s.starts_with("\"") && s.ends_with("\"") {
            Ok(Param::String(s.replace('"', "")))
        } else {
            Err(ParamFromStrError)
        }
    }
}

#[derive(Debug, Default)]
struct Flags {
    pub debug: bool,
    pub panic: bool,
    pub compiling: bool,
    mode: StackMode,
}

#[derive(Debug, Default)]
struct Stacks {
    pub mode_stack: Vec<StackMode>,
    pub param_stack: Vec<Param>,
    pub word_stack: Vec<Word>,
    pub estack: Vec<usize>,
}

fn main() -> rustyline::Result<()> {
    println!("=== Diplo v0.0.1 ===");

    let mut rl: Editor<(), SQLiteHistory> = {
        let config = Config::builder().auto_add_history(true).build();
        let history = SQLiteHistory::open(config, "history.sqlite3")?;
        Editor::with_history(config, history)?
    };

    let mut flags = Flags::default();
    let mut stacks = Stacks::default();
    let mut exec_count: usize = 0;
    let mut leftovers: usize = 0;
    let mut dict = Dict::default();

    dict.push(Word::prim_immediate("[", Instr::StartBlock));
    dict.push(Word::prim_immediate("|", Instr::StartWordMode));
    dict.push(Word::new(
        "[|",
        vec![Instr::StartBlock, Instr::StartWordMode],
        true,
    ));
    dict.push(Word::prim_immediate("]", Instr::EndBlock));
    dict.push(Word::prim_immediate("{", Instr::StartQuote));
    dict.push(Word::prim_immediate("}", Instr::EndQuote));
    dict.push(Word::prim_immediate(":", Instr::DefQuote));
    dict.push(Word::prim("showstacks", Instr::ShowStacks));
    dict.push(Word::prim("clearstacks", Instr::ClearStacks));
    dict.push(Word::prim("print", Instr::Print));
    dict.push(Word::prim("read", Instr::Read));
    dict.push(Word::prim("prompt", Instr::Prompt));
    dict.push(Word::prim("+", Instr::Add));
    dict.push(Word::prim("-", Instr::Sub));
    dict.push(Word::prim("*", Instr::Mul));
    dict.push(Word::prim("/", Instr::Div));
    dict.push(Word::prim("%", Instr::Mod));
    dict.push(Word::prim("^", Instr::Pow));
    dict.push(Word::prim("do", Instr::Do));
    dict.push(Word::prim("twice", Instr::Twice));
    dict.push(Word::prim("drop", Instr::Drop));
    dict.push(Word::prim("dup", Instr::Dup));
    dict.push(Word::prim("swap", Instr::Swap));
    dict.push(Word::prim("over", Instr::Over));
    dict.push(Word::prim("rot", Instr::Rot));
    dict.push(Word::prim("nip", Instr::Nip));
    dict.push(Word::prim("tuck", Instr::Tuck));
    dict.push(Word::prim("dotimes", Instr::Dotimes));
    dict.push(Word::prim("range", Instr::Range));

    'vm: loop {
        if flags.panic {
            println!("Encountered error: resetting interpreter.");
            flags.debug = false;
            flags.panic = false;
            flags.compiling = false;
            flags.mode = StackMode::default();
            stacks.mode_stack = Vec::new();
            stacks.param_stack = Vec::new();
            stacks.word_stack = Vec::new();
            exec_count = 0;
            stacks.estack = Vec::new();
        }

        let input = rl
            .readline(if flags.compiling { "  " } else { "> " })?
            .trim()
            .to_string();
        let mut parser = Parser::new(input);

        'interpreting: while let Ok(Some(token)) = parser.consume_word() {
            let token = token.text.as_str();

            if token == "debug" {
                flags.debug = !flags.debug;
                dbg!(flags.debug);
                continue 'interpreting;
            }

            match dict.lookup(token) {
                Some(word) => {
                    if word.immediate {
                        if execute_word(
                            &mut rl,
                            &mut flags,
                            &mut stacks,
                            &mut dict,
                            &mut parser,
                            &mut leftovers,
                            &mut exec_count,
                            &word,
                        )
                        .is_err()
                        {
                            continue 'vm;
                        } else {
                            continue 'interpreting;
                        }
                    }

                    if !flags.compiling {
                        println!("=> {word:?}");
                        continue 'interpreting;
                    }

                    match flags.mode {
                        StackMode::Param => stacks.param_stack.push(Param::Word(word.clone())),
                        StackMode::Word => {
                            stacks.word_stack.push(word.clone());
                            exec_count += 1;
                        }
                    }
                }
                None => match flags.compiling {
                    true => {
                        if flags.mode == StackMode::Word {
                            println!("E: unrecognized word {token}");
                            flags.panic = true;
                            continue 'vm;
                        }

                        let Ok(value) = Param::from_str(token) else {
                            println!("E: unrecognized word {token}");
                            flags.panic = true;
                            continue 'vm;
                        };

                        stacks.param_stack.push(value);
                    }
                    false => {
                        let Ok(value) = Param::from_str(token) else {
                            println!("E: Unrecognized token {}", token);
                            flags.panic = true;
                            continue 'vm;
                        };
                        println!("=> {value}");
                    }
                },
            }
        }

        if !flags.compiling {
            println!("");
        }
    }
}

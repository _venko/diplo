use itertools::Itertools;

#[derive(Debug)]
pub struct Parser {
    pub input: Vec<char>,
    pub cursor: usize,
}

impl Parser {
    pub fn new(input: String) -> Parser {
        Parser {
            input: input.chars().collect_vec(),
            cursor: 0,
        }
    }

    fn at_eoi(&self) -> bool {
        self.cursor == self.input.len()
    }

    fn skip_whitespace(&mut self) {
        while !self.at_eoi() && self.input[self.cursor].is_whitespace() {
            self.cursor += 1;
        }
    }

    pub fn consume_word(&mut self) -> Option<String> {
        self.skip_whitespace();

        let start_idx = self.cursor;
        let in_string = !self.at_eoi() && self.input[start_idx] == '"';

        loop {
            if self.at_eoi() {
                break;
            }
            if in_string {
                if self.input[self.cursor].is_whitespace() && self.input[self.cursor - 1] == '"' {
                    break;
                }
            } else {
                if self.input[self.cursor].is_whitespace() {
                    break;
                }
            }
            self.cursor += 1;
        }

        (start_idx != self.cursor).then_some(self.input[start_idx..self.cursor].iter().collect())
    }
}

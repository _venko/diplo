use std::str::FromStr;

use rustyline::{error::ReadlineError, Editor};

use forth_parser::Parser;

use crate::{dictionary::Dict, Flags, Instr, Param, StackMode, Stacks, Word};

const LBRACE: &'static str = "{";
const RBRACE: &'static str = "}";
const PIPE: &'static str = "|";

pub enum ExecutionError {
    Rustyline(ReadlineError),
    StackUnderflow,
    StackTypeMismatch,
    Panic,
}

fn get_next_word(flags: &mut Flags, stacks: &mut Stacks) -> Result<Word, ExecutionError> {
    let Some(word) = stacks.word_stack.pop() else {
        println!("E: ran out of words");
        flags.panic = true;
        return Err(ExecutionError::Panic);
    };
    Ok(word)
}

fn pop_param(flags: &mut Flags, param_stack: &mut Vec<Param>) -> Result<Param, ExecutionError> {
    let Some(param) = param_stack.pop() else {
        println!("E: Stack underflow.");
        flags.panic = true;
        return Err(ExecutionError::StackUnderflow);
    };
    Ok(param)
}

fn pop_num(flags: &mut Flags, param_stack: &mut Vec<Param>) -> Result<f32, ExecutionError> {
    let param = pop_param(flags, param_stack)?;
    let Param::Num(n) = param else {
        println!("E: Popped value was not a number.");
        flags.panic = true;
        return Err(ExecutionError::StackTypeMismatch);
    };
    Ok(n)
}

fn pop_string(flags: &mut Flags, param_stack: &mut Vec<Param>) -> Result<String, ExecutionError> {
    let param = pop_param(flags, param_stack)?;
    let Param::String(s) = param else {
        println!("E: Popped value was not a string.");
        flags.panic = true;
        return Err(ExecutionError::StackTypeMismatch);
    };
    Ok(s)
}

#[allow(unused)]
fn pop_bool(flags: &mut Flags, param_stack: &mut Vec<Param>) -> Result<bool, ExecutionError> {
    let param = pop_param(flags, param_stack)?;
    let Param::Bool(b) = param else {
        println!("E: Popped value was not a boolean.");
        flags.panic = true;
        return Err(ExecutionError::StackTypeMismatch);
    };
    Ok(b)
}

fn pop_word(flags: &mut Flags, param_stack: &mut Vec<Param>) -> Result<Word, ExecutionError> {
    let param = pop_param(flags, param_stack)?;
    let Param::Word(w) = param else {
        println!("E: Popped value was not a word.");
        flags.panic = true;
        return Err(ExecutionError::StackTypeMismatch);
    };
    Ok(w.clone())
}

fn get_arithmetic_operands(
    flags: &mut Flags,
    stacks: &mut Stacks,
) -> Result<(f32, f32), ExecutionError> {
    let rhs = pop_num(flags, &mut stacks.param_stack)?;
    let lhs = pop_num(flags, &mut stacks.param_stack)?;
    Ok((lhs, rhs))
}

pub struct CompileQuoteError;

pub fn compile_quote(
    dict: &Dict,
    parser: &mut Parser,
    flags: &mut Flags,
) -> Result<Word, CompileQuoteError> {
    let mut mode = StackMode::default();
    let mut compile_stack: Vec<Word> = Vec::new();
    let mut instrs: Vec<Instr> = Vec::new();

    // TODO: Handle parser errors.
    while let Ok(Some(token)) = parser.consume_word() {
        match &token.text[..] {
            RBRACE => break,

            LBRACE => {
                let Ok(word) = compile_quote(dict, parser, flags) else {
                    println!("E: Failed to compile sub-quote.");
                    flags.panic = true;
                    return Err(CompileQuoteError);
                };
                match mode {
                    StackMode::Param => {
                        instrs.push(Instr::PushParam(Param::Word(word)));
                    }
                    StackMode::Word => {
                        compile_stack.push(word);
                    }
                }
            }

            PIPE => mode = mode.swap(),
            _ => match (mode, dict.lookup(&token.text)) {
                (StackMode::Param, Some(word)) => {
                    instrs.push(Instr::PushParam(Param::Word(word.clone())));
                }

                (StackMode::Param, None) => {
                    let param = Param::from_str(&token.text).map_err(|_| CompileQuoteError)?;
                    instrs.push(Instr::PushParam(param));
                }

                (StackMode::Word, Some(word)) => {
                    compile_stack.push(word.clone());
                }

                (StackMode::Word, None) => {
                    flags.panic = true;
                    return Err(CompileQuoteError);
                }
            },
        }
    }

    for word in compile_stack.into_iter().rev() {
        for instr in &word.code {
            instrs.push(instr.clone());
        }
    }
    let word = Word::new("noname", instrs, false);

    Ok(word)
}

pub fn execute_word<H, I>(
    rl: &mut Editor<H, I>,
    flags: &mut Flags,
    stacks: &mut Stacks,
    dict: &mut Dict,
    parser: &mut Parser,
    leftovers: &mut usize,
    exec_count: &mut usize,
    word: &Word,
) -> Result<(), ExecutionError>
where
    H: rustyline::Helper,
    I: rustyline::history::History,
{
    use ExecutionError::*;

    for instr in word.code.iter() {
        match instr {
            Instr::PushParam(param) => {
                stacks.param_stack.push(param.clone());
            }

            Instr::PushWord(word) => stacks.word_stack.push(word.clone()),

            Instr::StartWordMode => {
                if flags.mode == StackMode::Word {
                    println!("E: Attempted to enter Word mode while already in Word mode.");
                    flags.panic = true;
                    return Err(Panic);
                }
                flags.mode = StackMode::Word;
            }

            Instr::StartBlock => match flags.compiling {
                true => {
                    stacks.mode_stack.push(flags.mode);
                    flags.mode = StackMode::default();
                    stacks.estack.push(*exec_count);
                    *exec_count = 0;
                }
                false => {
                    flags.compiling = true;
                    flags.mode = StackMode::default();
                }
            },

            Instr::EndBlock => match flags.compiling {
                true => {
                    if execute(rl, flags, stacks, dict, parser, exec_count, leftovers).is_err() {
                        println!("E: Failed in ]");
                        flags.panic = true;
                        return Err(Panic);
                    }
                }
                false => {
                    println!("E: Encountered ] outside []block");
                    flags.panic = true;
                    return Err(Panic);
                }
            },

            Instr::StartQuote => match flags.compiling {
                true => {
                    let Ok(word) = compile_quote(dict, parser, flags) else {
                        println!("E: Failed to compile quote.");
                        flags.panic = true;
                        return Err(Panic);
                    };
                    stacks.word_stack.push(word);
                    *exec_count += 1;
                }
                false => {
                    println!("E: Encountered }} outside a [] block.");
                    flags.panic = true;
                    return Err(Panic);
                }
            },

            Instr::EndQuote => unimplemented!("TODO: Implement }} as an immediate word."),

            Instr::DefQuote => {
                let Ok(Some(word_name)) = parser.consume_word() else {
                    println!("E: Failed to parse new word name.");
                    flags.panic = true;
                    return Err(Panic);
                };

                let Ok(Some(next_token)) = parser.consume_word() else {
                    println!("E: Failed to parse word definition.");
                    flags.panic = true;
                    return Err(Panic);
                };

                if &next_token.text != "{" {
                    println!("E: Failed to parse quote sigil. Found {next_token:?}.");
                    flags.panic = true;
                    return Err(Panic);
                }

                let Ok(compiled) = compile_quote(dict, parser, flags) else {
                    println!("E: Failed to compile word definition.");
                    flags.panic = true;
                    return Err(Panic);
                };

                dict.push(Word {
                    name: word_name.text,
                    ..compiled
                });
            }

            Instr::ShowStacks => {
                println!(
                    "stacks.pstack: {:?}\nstacks.wstack: {:?}",
                    stacks.param_stack, stacks.word_stack
                );
            }

            Instr::ClearStacks => {
                stacks.param_stack = Vec::new();
                stacks.word_stack = Vec::new();
            }

            Instr::Print => {
                let top = pop_param(flags, &mut stacks.param_stack)?;
                println!("{top}");
            }

            // TODO: Make this pull from the parser stream maybe?
            Instr::Read => {
                let input = rl.readline("").map_err(|err| Rustyline(err))?;
                let Ok(param) = Param::from_str(&input) else {
                    println!("E: Could not parse input as parameter");
                    flags.panic = true;
                    return Err(Panic);
                };
                stacks.param_stack.push(param);
            }

            Instr::Prompt => {
                let prompt = pop_string(flags, &mut stacks.param_stack)?;
                let input = rl.readline(&prompt).map_err(|err| Rustyline(err))?;
                let Ok(param) = Param::from_str(&input) else {
                    println!("E: Could not parse input as parameter");
                    flags.panic = true;
                    return Err(Panic);
                };
                stacks.param_stack.push(param);
            }

            Instr::Add => {
                let (lhs, rhs) = get_arithmetic_operands(flags, stacks)?;
                stacks.param_stack.push(Param::Num(lhs + rhs));
            }

            Instr::Sub => {
                let (lhs, rhs) = get_arithmetic_operands(flags, stacks)?;
                stacks.param_stack.push(Param::Num(lhs - rhs));
            }

            Instr::Mul => {
                let (lhs, rhs) = get_arithmetic_operands(flags, stacks)?;
                stacks.param_stack.push(Param::Num(lhs * rhs));
            }

            Instr::Div => {
                let (lhs, rhs) = get_arithmetic_operands(flags, stacks)?;
                stacks.param_stack.push(Param::Num(lhs / rhs));
            }

            Instr::Mod => {
                let (lhs, rhs) = get_arithmetic_operands(flags, stacks)?;
                if lhs.fract() != 0. || rhs.fract() != 0. {
                    println!(
                        "E: found fractional values in modulus operation: lhs={lhs} rhs={rhs}"
                    );
                    flags.panic = true;
                    return Err(ExecutionError::Panic);
                }
                stacks
                    .param_stack
                    .push(Param::Num((lhs as usize % rhs as usize) as f32));
            }

            Instr::Pow => {
                let (lhs, rhs) = get_arithmetic_operands(flags, stacks)?;
                stacks.param_stack.push(Param::Num(lhs.powf(rhs)));
            }

            Instr::Do => {
                let word = pop_word(flags, &mut stacks.param_stack)?;
                stacks.word_stack.push(word);
                *leftovers += 1;
            }

            Instr::Twice => {
                let word = pop_word(flags, &mut stacks.param_stack)?;
                stacks.word_stack.push(word.clone());
                stacks.word_stack.push(word);
                *leftovers += 2;
            }

            Instr::Dotimes => {
                let n = pop_num(flags, &mut stacks.param_stack)? as usize;
                let word = pop_word(flags, &mut stacks.param_stack)?;
                for _ in 0..n {
                    stacks.word_stack.push(word.clone());
                }
                *leftovers += n;
            }

            Instr::Dup => {
                let Some(p) = stacks.param_stack.last() else {
                    println!("E: Stack underflow.");
                    flags.panic = true;
                    return Err(StackUnderflow);
                };
                stacks.param_stack.push(p.clone());
            }

            Instr::Drop => {
                pop_param(flags, &mut stacks.param_stack)?;
            }

            Instr::Swap => {
                let len = stacks.param_stack.len();
                if len < 2 {
                    println!("E: Expected 2 or more param stack elements, found {len}");
                    flags.panic = true;
                    return Err(Panic);
                }
                stacks.param_stack.swap(len - 1, len - 2);
            }

            Instr::Over => {
                let len = stacks.param_stack.len();
                if len < 2 {
                    println!("E: Expected 2 or more param stack elements, found {len}");
                    flags.panic = true;
                    return Err(Panic);
                }
                stacks.param_stack.push(stacks.param_stack[len - 2].clone());
            }

            Instr::Rot => {
                let len = stacks.param_stack.len();
                if len < 3 {
                    println!("E: Expected 3 or more param stack elements, found {len}");
                    flags.panic = true;
                    return Err(Panic);
                }
                stacks.param_stack.swap(len - 1, len - 3);
                stacks.param_stack.swap(len - 3, len - 2);
            }

            Instr::Nip => {
                let len = stacks.param_stack.len();
                if len < 2 {
                    println!("E: Expected 2 or more param stack elements, found {len}");
                    flags.panic = true;
                    return Err(Panic);
                }
                stacks.param_stack.swap_remove(len - 2);
            }

            Instr::Tuck => {
                let len = stacks.param_stack.len();
                if len < 2 {
                    println!("E: Expected 2 or more param stack elements, found {len}");
                    flags.panic = true;
                    return Err(Panic);
                }
                stacks
                    .param_stack
                    .insert(len - 2, stacks.param_stack.last().unwrap().clone());
            }

            Instr::Range => {
                let hi = pop_num(flags, &mut stacks.param_stack)? as usize;
                let lo = pop_num(flags, &mut stacks.param_stack)? as usize;
                for i in lo..hi {
                    stacks.param_stack.push(Param::Num(i as f32));
                }
            }
        }
    }

    Ok(())
}

pub fn execute<H, I>(
    rl: &mut Editor<H, I>,
    flags: &mut Flags,
    stacks: &mut Stacks,
    dict: &mut Dict,
    parser: &mut Parser,
    exec_count: &mut usize,
    leftovers: &mut usize,
) -> Result<(), ExecutionError>
where
    H: rustyline::Helper,
    I: rustyline::history::History,
{
    for _ in 0..*exec_count {
        let word = get_next_word(flags, stacks)?;
        execute_word(
            rl, flags, stacks, dict, parser, leftovers, exec_count, &word,
        )?;
    }

    if !stacks.mode_stack.is_empty() {
        flags.mode = stacks.mode_stack.pop().unwrap();
        *exec_count = stacks.estack.pop().unwrap() + *leftovers;
        *leftovers = 0;
    } else {
        flags.mode = StackMode::default();
        *exec_count = 0;
        flags.compiling = false;
    }

    Ok(())
}

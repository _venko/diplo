use itertools::Itertools;

use crate::Param;

#[derive(Debug, Clone, PartialEq)]
pub enum Instr {
    PushParam(Param),
    PushWord(Word),
    StartWordMode,
    StartBlock,
    EndBlock,
    StartQuote,
    EndQuote,
    DefQuote,

    // Debug
    ShowStacks,
    ClearStacks,

    // IO
    Print,
    Read,
    Prompt,

    // Arithmetic
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    Pow,

    // Combinators
    Do,
    Twice,
    Dotimes,

    // Stack manipulation
    Drop,
    Dup,
    Swap,
    Over,
    Rot,
    Nip,
    Tuck,

    // Misc
    Range,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Word {
    pub name: String,
    pub code: Vec<Instr>,
    pub immediate: bool,
}

impl Word {
    pub fn new(name: impl Into<String>, instrs: Vec<Instr>, immediate: bool) -> Word {
        Word {
            name: name.into(),
            code: instrs.into_iter().collect_vec(),
            immediate,
        }
    }

    pub fn prim(name: impl Into<String>, instr: Instr) -> Word {
        Word::new(name, vec![instr], false)
    }

    pub fn prim_immediate(name: impl Into<String>, instr: Instr) -> Word {
        Word::new(name, vec![instr], true)
    }
}

#[derive(Debug, Default)]
pub struct Dict(Vec<Word>);

impl Dict {
    pub fn push(&mut self, word: Word) {
        self.0.push(word);
    }

    pub fn lookup(&self, token: &str) -> Option<Word> {
        self.0.iter().rev().find(|w| w.name == token).cloned()
    }
}
